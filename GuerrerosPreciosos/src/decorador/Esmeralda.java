package decorador;

import componente.GuanteDeGemas;

public class Esmeralda extends GemaDecoradora {
	GuanteDeGemas guante;
	
	public Esmeralda(GuanteDeGemas guante) {
		this.guante = guante;
	}
	
	@Override
	public String getDescripcion() {
		// TODO Auto-generated method stub
		return guante.getDescripcion() + ", Esmeralda";
	}

	@Override
	public int poder() {
		// TODO Auto-generated method stub
		return 2 + guante.poder();
	}

}
