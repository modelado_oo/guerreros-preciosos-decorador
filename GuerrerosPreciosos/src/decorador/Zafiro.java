package decorador;

import componente.GuanteDeGemas;

public class Zafiro extends GemaDecoradora {
	GuanteDeGemas guante;
	
	public Zafiro(GuanteDeGemas guante) {
		this.guante = guante;
	}
	
	@Override
	public String getDescripcion() {
		// TODO Auto-generated method stub
		return guante.getDescripcion() + ", Zafiro";
	}

	@Override
	public int poder() {
		// TODO Auto-generated method stub
		return 3 + guante.poder();
	}

}
