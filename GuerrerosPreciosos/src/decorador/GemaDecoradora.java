package decorador;

import componente.GuanteDeGemas;

public abstract class GemaDecoradora extends GuanteDeGemas {
	public abstract String getDescripcion();
}
