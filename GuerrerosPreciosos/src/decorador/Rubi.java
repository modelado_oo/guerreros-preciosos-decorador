package decorador;

import componente.GuanteDeGemas;

public class Rubi extends GemaDecoradora {
	GuanteDeGemas guante;
	
	public Rubi(GuanteDeGemas guante) {
		this.guante = guante;
	}
	
	@Override
	public String getDescripcion() {
		// TODO Auto-generated method stub
		return guante.getDescripcion() + ", Rubí";
	}

	@Override
	public int poder() {
		// TODO Auto-generated method stub
		return 5 + guante.poder();
	}

}
