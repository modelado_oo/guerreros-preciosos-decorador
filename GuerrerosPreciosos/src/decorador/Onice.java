package decorador;

import componente.GuanteDeGemas;

public class Onice extends GemaDecoradora {
	GuanteDeGemas guante;
	
	public Onice(GuanteDeGemas guante) {
		this.guante = guante;
	}
	
	@Override
	public String getDescripcion() {
		// TODO Auto-generated method stub
		return guante.getDescripcion() + ", Onice";
	}

	@Override
	public int poder() {
		// TODO Auto-generated method stub
		return 1 + guante.poder();
	}

}
