package decorador;

import componente.GuanteDeGemas;

public class Diamante extends GemaDecoradora {
	GuanteDeGemas guante;
	
	public Diamante(GuanteDeGemas guante) {
		this.guante = guante;
	}
	
	@Override
	public String getDescripcion() {
		// TODO Auto-generated method stub
		return guante.getDescripcion() + ", Diamante";
	}

	@Override
	public int poder() {
		// TODO Auto-generated method stub
		return 8 + guante.poder();
	}

}
