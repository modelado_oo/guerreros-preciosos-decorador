package pelea;

import juego.EstadoJugador;
import juego.Jugador;
import juegoGuerreros.GuerreroDeGemas;

public class ResultadoPeleaSimple implements ResultadoPelea {
	
	private GuerreroDeGemas retador;
	private GuerreroDeGemas contrincante;
	private GuerreroDeGemas ganador;
	private GuerreroDeGemas perdedor;
	private boolean empate = false;
	
	public ResultadoPeleaSimple(GuerreroDeGemas retador, GuerreroDeGemas contrincante, boolean empate) {
		this.retador = retador;
		this.contrincante = contrincante;
		this.empate = empate;
	}
	
	public ResultadoPeleaSimple(GuerreroDeGemas retador, GuerreroDeGemas contrincante, GuerreroDeGemas ganador, GuerreroDeGemas perdedor) {
		this.retador = retador;
		this.contrincante = contrincante;
		this.ganador = ganador;
		this.perdedor = perdedor;
		
		if (perdedor.getPoderBatalla() <= 0) {
			perdedor.setEstadoJugador(EstadoJugador.MUERTO);
		}
	}
	
	public String toString() {
		String str = "La pelea entre " + retador.getNombre() + " VS " + contrincante.getNombre();
		if (empate) {
			str += " resultó en empate.";
		} else {
			str += " resultó en victoria de: " + ganador.getNombre();
		}
		
		return str;
//		 return "[" + retador + "] VS [" + contrincante + "]" + " = " + ganador.getNombre() + "\n"
//				+"[Guerrero Ganador:" + ganador.getNombre()+ "]"  + "[Poder de Batalla: " + ganador.getPoder()+"]";
	}

	public GuerreroDeGemas getRetador() {
		return retador;
	}

	public void setRetador(GuerreroDeGemas retador) {
		this.retador = retador;
	}

	public GuerreroDeGemas getContrincante() {
		return contrincante;
	}

	public void setContrincante(GuerreroDeGemas contrincante) {
		this.contrincante = contrincante;
	}

	public GuerreroDeGemas getGanador() {
		return ganador;
	}

	public void setGanador(GuerreroDeGemas ganador) {
		this.ganador = ganador;
	}

	public GuerreroDeGemas getPerdedor() {
		return perdedor;
	}

	public void setPerdedor(GuerreroDeGemas perdedor) {
		this.perdedor = perdedor;
	}

	public boolean isEmpate() {
		return empate;
	}

	public void setEmpate(boolean empate) {
		this.empate = empate;
	}
	
}
