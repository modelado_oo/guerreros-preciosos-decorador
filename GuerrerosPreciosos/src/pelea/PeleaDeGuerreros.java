package pelea;

import juegoGuerreros.GuerreroDeGemas;

public class PeleaDeGuerreros implements Pelea {
	private GuerreroDeGemas retador;
	private GuerreroDeGemas contrincante;

	public PeleaDeGuerreros(GuerreroDeGemas retadorGemas, GuerreroDeGemas contrincanteGemas) {
		retador=retadorGemas;
		contrincante= contrincanteGemas;
	}

	@Override
	public ResultadoPeleaSimple pelear() {
		ResultadoPeleaSimple resultado;
		
		if(retador.getPoderBatalla()>contrincante.getPoderBatalla()) {
			int dmg = retador.getPoderBatalla()-contrincante.getPoderBatalla();
				
			contrincante.dmgAcumulado += dmg;
			resultado = new ResultadoPeleaSimple(retador, contrincante, retador, contrincante);
		} else if (retador.getPoderBatalla()<contrincante.getPoderBatalla()) {
			int dmg = contrincante.getPoderBatalla()-retador.getPoderBatalla();
			
			retador.dmgAcumulado += dmg;
			resultado = new ResultadoPeleaSimple(retador, contrincante, contrincante, retador);
		} else {
			resultado = new ResultadoPeleaSimple(retador, contrincante, true);
		}
		
		return resultado;
	}
	
	public String toString() {
		return "Pelea de " + retador + " VS " + contrincante;  
	}
}
