package juego;

import java.util.Random;

public abstract class Dado {
	protected static final Random RANDOM = new Random();
	
	public abstract int lanzar();
}
