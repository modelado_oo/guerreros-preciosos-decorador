package juego;

public class DadoSimple extends Dado{
	private int valorMaximo;
	private boolean cero;
	
	public DadoSimple(int valorMaximo, boolean cero) {
		this.valorMaximo = valorMaximo;
		this.cero = cero;
	}

	@Override
	public int lanzar() {
		return !cero ? RANDOM.nextInt(valorMaximo - 1) + 1 : 
			RANDOM.nextInt(valorMaximo + 1);
	}
	
	public String toString() {
		return "Dado: valor máximo, " + valorMaximo + "; ¿Incluye cero?" + cero;
	}
}
