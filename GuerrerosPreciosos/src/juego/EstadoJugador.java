package juego;

public enum EstadoJugador {
	VIVO,
	MUERTO;
	
	public String toString() {
		return name().substring(0, 1) + name().substring(1).toLowerCase();
	}
}
