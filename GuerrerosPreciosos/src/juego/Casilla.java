package juego;

import java.util.ArrayList;
import java.util.List;

public abstract class Casilla {
	protected int numero;
	protected List<Jugador> jugadores;
	
	public Casilla(int numero) {
		this.numero = numero;
		jugadores = new ArrayList<>();
	}
	
	public String toString() {
		String s;
		
		if (jugadores.size() > 0) {
			s = jugadores.toString();
		} else {
			s = "Ningun jugador";
		}
		
		return "Casilla #" + numero + ": " + s + ".";
	}
	
	public List<Jugador> getJugadores() {
		return jugadores;
	}
	
	public int getNumero() {
		return numero;
	}
	
	public void addJugador(Jugador jugador) {
		jugadores.add(jugador);
	}
	
	public void removeJugador(Jugador jugador) {
		jugadores.remove(jugador);
	}
}
