package juego;

public abstract class Jugador {
	protected String nombre;
	//protected Tablero tablero;
	protected EstadoJugador estado = EstadoJugador.VIVO;
	
	public Jugador(String nombre) {
		this.nombre = nombre;
	}
	
	public Jugador(String nombre, Tablero tablero) {
		this.nombre = nombre;
		//this.tablero = tablero;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public EstadoJugador getEstadoJugador() {
		return estado;
	}
	
	public void setEstadoJugador(EstadoJugador estado) {
		this.estado = estado;
	}
	
	public String toString() {
		return "Jugador '" + nombre + "' {" + estado + "}"; //en el tablero " + tablero + ".";
	}
}
