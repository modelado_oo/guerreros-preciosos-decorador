package juego;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public abstract class Tablero {
	protected Casilla[] casillas;
	
	public abstract void reiniciar();
	
	public abstract void dibujar();
	
	public Casilla[] getCasillas() {
		return casillas;
	}
	
	public int size() {
		return casillas.length - 1;
	}
	
	public Casilla getCasilla(int numero) {
		if (numero > casillas.length || numero < 0) {
			throw new IllegalArgumentException("Casilla inválida.");
		}
		
		return casillas[numero];
	}
	
	public int posicionJugador(Jugador jugadorBuscado) {
		return Arrays.stream(casillas)
				.filter(casilla -> casilla.getJugadores()
						.stream()
						.anyMatch(jugador -> jugador.getNombre().equals(jugadorBuscado.getNombre())))
				.findFirst()
				.get()
				.getNumero();
	}
	
	@SuppressWarnings("unchecked")
	public <T extends Jugador> T encontrarJugador(T jugadorBuscado) {
		return (T) Arrays.stream(casillas)
				.map(casilla -> casilla.getJugadores()
						.stream()
						.filter(jugador -> jugador.getNombre().equals(jugadorBuscado.getNombre()))
						.findFirst()
						.get());
						
	}
	
	public List<Jugador> getJugadores() {
		return Arrays.stream(casillas)
				.map(Casilla::getJugadores)
				.flatMap(List::stream)
				.collect(Collectors.toList());
	}
	
	public String toString() {
		return Arrays.toString(casillas);
	}

	public boolean isSoloUnJugadorVivo() {
		return getJugadores().stream()
				.filter(jugador -> jugador.getEstadoJugador() == EstadoJugador.VIVO)
				.count() == 1;
	}
	
	public boolean llegoAlFinal(Jugador jugadorBuscado) {
		return posicionJugador(jugadorBuscado) == casillas.length - 1;
	}

	public boolean isFinDelCamino() {
		return getJugadores().stream()
				.map(jugador -> posicionJugador(jugador))
				.allMatch(posicion -> posicion== casillas.length-1);
	}
}
