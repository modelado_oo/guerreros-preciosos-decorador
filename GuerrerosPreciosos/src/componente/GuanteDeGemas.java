package componente;

public abstract class GuanteDeGemas {
	protected String descripcion = "Sin guante";
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public abstract int poder();
	
	public String toString() {
		return getDescripcion() + " {" + poder() + "}";
	}
}
