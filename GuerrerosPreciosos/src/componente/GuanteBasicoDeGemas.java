package componente;

public class GuanteBasicoDeGemas extends GuanteDeGemas {

	public GuanteBasicoDeGemas() {
		descripcion = "Guante básico de gemas";
	}

	@Override
	public int poder() {
		return 4;
	}
	
}
