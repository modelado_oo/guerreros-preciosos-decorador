package juegoGuerreros;

import java.util.List;
import java.util.stream.Collectors;

import juego.Tablero;

public class TableroGuerreros extends Tablero {
	
	public TableroGuerreros(int numCasillas, int numMaxGemasPorCasilla) {
		casillas = new CasillaDeGemas[numCasillas];
		
		for(int i=0; i< numCasillas;i++) {
			casillas[i]=new CasillaDeGemas(i, numMaxGemasPorCasilla);
		}
	}

	@Override
	public void reiniciar() {
		
	}

	@Override
	public void dibujar() {
		
	}	
	
	public CasillaDeGemas[] getCasillas() {
		return (CasillaDeGemas[]) casillas;
	}
	
	public List<GuerreroDeGemas> getGuerrerosVivos() {
		return getJugadores().stream()
				.map(jugador -> (GuerreroDeGemas) jugador)
				.filter(GuerreroDeGemas::isVivo)
				.collect(Collectors.toList());
	}
	
	public CasillaDeGemas getCasilla(int numero) {
		return (CasillaDeGemas) super.getCasilla(numero);
	}
	
	/*
	 * Devuelve los guerreros vivos y que no han llegado al final del tablero
	 */
	public List<GuerreroDeGemas> getGuerrerosDisponibles() {
		return getGuerrerosVivos().stream()
				.filter(guerrero -> guerrero.isVivo() && !llegoAlFinal(guerrero) && posicionJugador(guerrero) != size())
				.collect(Collectors.toList());
	}
}
