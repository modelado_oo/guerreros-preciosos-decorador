package juegoGuerreros;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import juego.Casilla;

public class CasillaDeGemas extends Casilla {
	private List<Gema> gemas;
	private static final Random RANDOM = new Random();

	public CasillaDeGemas(int numero, int maxGemas) {
		super(numero);
		gemas = new ArrayList<>();
        int numeroGemasCasilla = RANDOM.nextInt(4);
        for (int i = 0; i < numeroGemasCasilla; i ++) {
			gemas.add(Gema.gemaAleatoria());
		}
	}

	public String toString() {
		return super.toString() + "; Gemas en casilla: " + gemas + ".";
	}
	
	public List<Gema> recolectarGemas() {
		List<Gema> gemasRecolectadas = new ArrayList<>();
		gemasRecolectadas.addAll(gemas);
		
		gemas.clear();
		return gemasRecolectadas;
	}
}
