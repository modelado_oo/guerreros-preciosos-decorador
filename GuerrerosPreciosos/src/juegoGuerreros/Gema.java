package juegoGuerreros;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum Gema {
	ESMERALDA(2),
	RUBI(5),
	ONICE(1),
	DIAMANTE(8),
	ZAFIRO(3);
	
	private int poder;
	
	private static final List<Gema> VALORES = Collections.unmodifiableList(Arrays.asList(values()));
	private static final int TAM = VALORES.size();
	private static final Random RANDOM = new Random();

	private Gema(int poder) {
		this.poder = poder;
	}
	
	public static Gema gemaAleatoria() {
		return VALORES.get(RANDOM.nextInt(TAM));
	}
	
	public static Gema getGema(int val) {
		return VALORES.get(val);
	}
	
	public int getPoder() {
		return poder;
	}
	
	public String toString() {
		return name().substring(0, 1) + name().substring(1).toLowerCase() + " (" + poder + ")";
	}
}
