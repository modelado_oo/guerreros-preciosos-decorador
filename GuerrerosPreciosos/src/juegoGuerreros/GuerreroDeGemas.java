package juegoGuerreros;

import componente.GuanteBasicoDeGemas;
import componente.GuanteDeGemas;
import decorador.Diamante;
import decorador.Esmeralda;
import decorador.Onice;
import decorador.Rubi;
import decorador.Zafiro;
import juego.EstadoJugador;
import juego.Jugador;

public class GuerreroDeGemas extends Jugador {
	private GuanteDeGemas guante;
	public int dmgAcumulado=0;
	protected int poderBatalla=0;
	public int posicion=0;
	
	public GuerreroDeGemas(String nombre) {
		super(nombre);
		guante = new GuanteBasicoDeGemas(); // Se inicializa el componente basico
	}

	public int getPoder() {
		return guante.poder();
	}
	
	public void equiparGema(Gema gema) { // Metodo para decorar el componente
		if (gema == null)
			return;
		
		switch (gema) {
		case DIAMANTE:
			guante = new Diamante(guante);
			break;
		case ESMERALDA:
			guante = new Esmeralda(guante);
			break;
		case ONICE:
			guante = new Onice(guante);
			break;
		case RUBI:
			guante = new Rubi(guante);
			break;
		case ZAFIRO:
			guante = new Zafiro(guante);
			break;
		default:
			// No puede pasar
			break;
		}
	}

	public void quitarGuante() {
		guante = new GuanteBasicoDeGemas();
	}
	
	public String toString() {
		return "Guerrero de gemas " + nombre + " {" + guante.poder() + "}" + "[Poder de Batalla: "+ this.getPoderBatalla()+"]";
	}

	public GuanteDeGemas getGuante() {
		return guante;
	}

	public void setGuante(GuanteDeGemas guante) {
		this.guante = guante;
	}

	public int getDmgAcumulado() {
		return dmgAcumulado;
	}

	public int getPoderBatalla() {
		return getPoder() - dmgAcumulado;
	}

	public int getPosicion() {
		return posicion;
	}

	public void setPosicion(int posicion) {
		this.posicion = posicion;
	}

	public boolean isVivo() {
		return estado == EstadoJugador.VIVO;
	}
}
