package Tests;

import juegoGuerreros.Gema;
import juegoGuerreros.GuerreroDeGemas;

/**
 * 
 * @author ropoja
 *
 * Clase de pruebas de código en general.
 */
public class BrutalTests {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("¡Hola Mundo!");
		GuerreroDeGemas guerrero = new GuerreroDeGemas("Prueba");
		System.out.println(guerrero);
		System.out.println(guerrero.getGuante());
		guerrero.equiparGema(Gema.gemaAleatoria());
		System.out.println(guerrero);
		System.out.println(guerrero.getGuante());
		guerrero.equiparGema(Gema.gemaAleatoria());
		System.out.println(guerrero);
		System.out.println(guerrero.getGuante());
		guerrero.equiparGema(Gema.gemaAleatoria());
		System.out.println(guerrero);
		System.out.println(guerrero.getGuante());
	}

}
