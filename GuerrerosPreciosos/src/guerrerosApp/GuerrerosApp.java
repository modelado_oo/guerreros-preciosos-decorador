package guerrerosApp;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import juego.Dado;
import juego.DadoSimple;
import juego.Jugador;
import juego.Tablero;
import juegoGuerreros.CasillaDeGemas;
import juegoGuerreros.GuerreroDeGemas;
import juegoGuerreros.TableroGuerreros;
import pelea.PeleaDeGuerreros;
import pelea.ResultadoPelea;
import pelea.ResultadoPeleaSimple;

public class GuerrerosApp {
	private static int MAX_GEMAS = 3;
	private static int NUM_JUGADORES = 3;
	private static int NUM_CASILLAS_TABLERO = 20;
	private static int NUM_TURNOS = 20; 
	private static int VALOR_MAX_DADO = 3;
	private static boolean CERO_EN_DADO = false;
	public static Dado DADO = new DadoSimple(VALOR_MAX_DADO, CERO_EN_DADO);
	private static final Random RANDOM = new Random();

	private TableroGuerreros tableroGuerrero;
	private List<GuerreroDeGemas> jugadores;
	private Jugador jugadorEnTurno;
	private CasillaDeGemas casillaActual;

	public GuerrerosApp(int tamTablero, int numJugadores, int maxGemas) {
		tableroGuerrero = new TableroGuerreros(tamTablero, maxGemas);
		jugadores = new ArrayList<>();
		jugadores.add(new GuerreroDeGemas("Apolo"));
		jugadores.add(new GuerreroDeGemas("Kaizen"));
		jugadores.add(new GuerreroDeGemas("Thanos"));
	}

	public void iniciarJuego() {
		GuerreroDeGemas Guerrero1 = jugadores.get(0);
		GuerreroDeGemas Guerrero2 = jugadores.get(1);
		GuerreroDeGemas Guerrero3 = jugadores.get(2);
		casillaActual = tableroGuerrero.getCasilla(0);
		casillaActual.addJugador(Guerrero1);
		casillaActual.addJugador(Guerrero2);
		casillaActual.addJugador(Guerrero3);

		int turnoActual = 1;
		
		while (!isJuegoTerminado()) {
			
			if(turnoActual < NUM_TURNOS) {
				List<GuerreroDeGemas> guerrerosDisponibles = tableroGuerrero.getGuerrerosDisponibles();
				
				for (GuerreroDeGemas guerrero : guerrerosDisponibles) {
					if (!tableroGuerrero.isSoloUnJugadorVivo()) {
						jugarTurno(guerrero);
					}
				}
				
				System.out.println("Fin del turno " + turnoActual);
				turnoActual++;
			}
		}
		
		System.out.println("Juego terminado.");
	}
	
	public int moverGuerrero(GuerreroDeGemas guerrero, int cantidadMovimiento) {
		int posicionActual = tableroGuerrero.posicionJugador(guerrero);
		int posicionDestino = posicionActual + cantidadMovimiento;
		
		if (posicionDestino > tableroGuerrero.getCasillas().length-1) {
			posicionDestino = tableroGuerrero.getCasillas().length-1;
		}
		
		CasillaDeGemas casillaOrigen = tableroGuerrero.getCasillas()[posicionActual];
		casillaOrigen.removeJugador(guerrero);
		
		CasillaDeGemas casillaDestino = tableroGuerrero.getCasillas()[posicionDestino];
		casillaDestino.addJugador(guerrero);
		
		return posicionDestino;
	}
	
	public void jugarTurno(GuerreroDeGemas guerrero) {
		casillaActual = tableroGuerrero.getCasilla(tableroGuerrero.posicionJugador(guerrero));
		
		int resultadoDado = DADO.lanzar();
		
		System.out.println("El guerrero " + guerrero.getNombre() + " a lanzado un " + resultadoDado);
		
		int posicionDestino = moverGuerrero(guerrero, resultadoDado);
		
		System.out.println("El guerrero " + guerrero.getNombre() + " se mueve de la casilla " + casillaActual.getNumero() + " a la casilla " + posicionDestino);		
		
		casillaActual = tableroGuerrero.getCasilla(posicionDestino);
		
		casillaActual.recolectarGemas().stream()
			.forEach(gema -> { 
				guerrero.equiparGema(gema);
				System.out.println(guerrero + " se ha equipado la gema " + gema + ".");
			});
		
		if(casillaActual.getJugadores().size()>1) {
			casillaActual.getJugadores().stream().filter(jugadorContrincante->!jugadorContrincante.equals(guerrero)).
				forEach(jugadorContrincante->{
					GuerreroDeGemas guerreroContrincante = (GuerreroDeGemas)jugadorContrincante;
					if(guerrero.isVivo()) {
						PeleaDeGuerreros batallaRealizar = new PeleaDeGuerreros(guerrero,guerreroContrincante);
						
						System.out.println(batallaRealizar);
						
						ResultadoPeleaSimple resultadoPelea = batallaRealizar.pelear();  
						
						System.out.println(resultadoPelea);
						
						if(!resultadoPelea.isEmpate()) {
							System.out.println("El poder de batalla del Guerrero: "+ resultadoPelea.getPerdedor().getNombre()+" Se a reducido a: "+ resultadoPelea.getPerdedor().getPoderBatalla());
						}
												
						if (!guerrero.isVivo()) {
							System.out.println("El guerrero "+ guerrero.getNombre()+" a sido eliminado...");
						} 
						
						if (!guerreroContrincante.isVivo()) {
							System.out.println("El guerrero " + guerrero.getNombre() + " a sido eliminado...");
						}
						
					}
				});
		}
		
		System.out.println("Fin del turno del Guerrero "+ guerrero.getNombre());
		
	}

	public boolean isJuegoTerminado() {
		
		boolean juegoTerminado=false;
		
		if(tableroGuerrero.isSoloUnJugadorVivo()) {
			System.out.println("El Guerrero ganador del juego es: ");
			System.out.println(tableroGuerrero.getGuerrerosVivos().toString());
			juegoTerminado = true;
		}
		
		if (tableroGuerrero.isFinDelCamino()) {
			GuerreroDeGemas guerreroGanador = tableroGuerrero.getGuerrerosVivos().stream()
				.filter(guerrero -> guerrero.getPoderBatalla() == (tableroGuerrero.getGuerrerosVivos().stream()
						.map(guerrero2 -> guerrero2.getPoderBatalla())
						.mapToInt(poder -> poder)
						.max().getAsInt()))
				.findFirst()
				.get();
		
			System.out.println("El Guerrero Ganador del juego es : "+ guerreroGanador.toString());
			juegoTerminado = true;
		}
		
		return juegoTerminado;
	}


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		GuerrerosApp app = new GuerrerosApp(NUM_CASILLAS_TABLERO, NUM_JUGADORES, MAX_GEMAS);
		System.out.println(app.tableroGuerrero);
		app.iniciarJuego();
	}

}
